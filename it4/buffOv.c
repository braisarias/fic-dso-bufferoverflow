#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define MAX 50
#define MAXCHAR 10
#define MAX_C 50
#define SALTO 10
#define DIR_RET 14
#define DIR_NEXT_FRAME 12
#define SALT_FRAME 80

#define F_STACK "stack.dat"
#define F_OUT "buffOv.log"

FILE *fLog;

void g()
{
	char c[MAX_C]={"ls"};
	f(5);
	system(c);
	return;
	
	
}


int f(int n){
	int i, fd;
	int a[MAXCHAR] = {111, 111, 111, 111, 111, 111, 111, 111, 111, 111};
	i = 0xFF;
	char input[4]={'A','a','A','a'};
	
	if ((fd = open(F_STACK, O_WRONLY | O_CREAT, 0700)) == -1){
		perror("Erro abrindo ficheiro");
		exit(-1);
	}

	if (write(fd, input, MAX*(sizeof(int))) == -1){
		perror("Erro escribindo ficheiro");
		exit(-1);
	}
	close(fd);

	fprintf(fLog, "antes scanf\n\n");
	for (i=-10; i< MAX; i++){
		fprintf(fLog, "%3d: [0x%x] %12d : 0x%x\n", i, &(a[i]), a[i], a[i]);
	}
	
	scanf("%[^\n]s", input);
	
	fprintf(fLog, "depois scanf\n\n");
	for (i=-10; i< MAX; i++){
		fprintf(fLog, "%3d: [0x%x] %12d : 0x%x\n", i, &(a[i]), a[i], a[i]);
	}
	
	return 0;
}


int main(int argc, char *argv[]){
	int a[MAXCHAR] = {1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010};
	
	// ficheiro de log, coa info da pila
	fLog = fopen(F_OUT, "w");
	if (fLog == NULL){
		perror("Erro abrindo ficheiro");
		exit(-1);
	}
	
	printf("print stack\n");
	
	g();

	printf("ultimo 7\n");
	return 0;
}
