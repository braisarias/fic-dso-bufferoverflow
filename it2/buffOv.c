#include <stdio.h>

#define MAX 100
#define MAXCHAR 10
#define SALTO 10
#define DIR_RET 14
#define DIR_NEXT_FRAME 12
#define SALT_FRAME 80

int f(int n, int n1, int n2, int n3){
	int i;
	int a[MAXCHAR];
	
	for(i=0; i<MAXCHAR; i++){
		a[i] = 101;
	}
	
	if (n != 0){
		f(n-1, n1-1, n2-1, n3-1);
	} else {
		printf("\na[%d] = 0x%x\n", DIR_NEXT_FRAME, a[DIR_NEXT_FRAME]);
		printf("\na[%d] + %d = 0x%x\n", DIR_NEXT_FRAME, SALT_FRAME, a[DIR_NEXT_FRAME]+SALT_FRAME);
		a[DIR_NEXT_FRAME] += SALT_FRAME;
		
			
		for (i=-1; i< MAX; i++){
			if (i==DIR_NEXT_FRAME){
				printf("-->");
			}
			printf("f1 [0x%x] -> %d : 0x%x\n", &(a[i]), a[i], a[i]);
		}
	}
	printf("n %d\n", n);
	return 0;
}


int main(){
	f(5,5,5,5);
	return 0;
}
